# nwnstatusbot
A Discord bot that shows the status of [Neverwinter Nights EE](https://www.beamdog.com/games/neverwinter-nights-enhanced/) servers by querying the [Beamdog API](https://api.nwn.beamdog.net/v1/).
Written in [Nim](https://nim-lang.org/).

It is based on the [dimscord](https://github.com/krisppurg/dimscord) library.

## Use it now
I have an instance of this bot running and you can invite it to your discord via this link https://discord.com/api/oauth2/authorize?client_id=731941439113658368&scope=bot&permissions=18432.

After inviting the bot type `!help` to get started.

## Screenshots
Default server status  
![status](status.png)

Verbose server status  
![verbose status](status-verbose.png)

## Run the bot yourself
* Install [Nim](https://nim-lang.org/)
* Clone this repository
* In the cloned directory run `nimble run` or `nimble build`

To start the compiled bot successfully you need a token from discord. Create an application and bot on discords web page. Copy the token for your bot.
Pass the token to the bot via the environment variable `TOKEN`.

Example: `TOKEN=SOMELONGTOKENHERE nimble run` or (after compiling a binary) `TOKEN=SOMELONGTOKENHERE ./nwnstatusbot`
