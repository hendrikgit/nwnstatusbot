# Package
version       = "0.1.0"
author        = "Hendrik Albers"
description   = "Discord bot that lists current players of a Neverwinter Nights server"
license       = "MIT"
srcDir        = "src"
bin           = @["nwnstatusbot"]

# Dependencies
requires "nim >= 1.2.4"
requires "dimscord == 1.2.1"
