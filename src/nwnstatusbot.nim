import algorithm, asyncdispatch, colors, httpClient, json, logging, options, os, sequtils, strformat, strutils, tables, times
import types
import dimscord

const
  statusCommands = ["!nwnstatus", "!status", "!s", "!players", "!p"]
  logFormat = "$date $time $levelname: "
  defaultServersFile = "default-servers.json"
  requiredPermissionsBot = {permSendMessages, permEmbedLinks}
  requiredPermissionsSetDefault = {permManageGuild}

addHandler(newConsoleLogger(fmtStr = logFormat))
addHandler(newRollingFileLogger(filename = "nwnstatusbot.log", mode = fmAppend, bufSize = 0, fmtStr = logFormat))

if not existsEnv("TOKEN"):
  error "Please set environment variable TOKEN to the bots token."
  quit(QuitFailure)
let botToken = getEnv("TOKEN")

info "Starting bot"

var defaultServers = initTable[string, string]()
if fileExists(defaultServersFile):
  info "Reading " & defaultServersFile
  let content = defaultServersFile.readFile
  let json = if content.strip.len > 0: content.parseJson else: newJArray()
  for e in json.elems:
    defaultServers[e["guild_id"].getStr] = e["server"].getStr
  info defaultServers

proc toJSON (t: Table[string, string]): JsonNode
proc send (api: RestApi, channel_id: string, content = "", embed = none Embed): Future[Option[Message]] {.async.}
proc getServerInfoAndReply (serverName: string, m: Message, verbose = false) {.async.}
proc getServerInfo (name: string): Future[Option[Server]] {.async.}
proc getServerInfoFromAPI (): Future[JsonNode] {.async.}
proc toYesNo (b: bool): string
proc toEmbed (server: Server, verbose = false): Embed
proc getGuild (guild_id: string, s: Shard): Future[Guild] {.async.}
proc getGuildMember(s: Shard, guild_id, user_id: string): Future[Member] {.async.}
proc originInfo (m: Message, s: Shard): Future[string] {.async.}

let cl = newDiscordClient(botToken)

cl.events.on_ready = proc (s: Shard, r: Ready) {.async.} =
  info "Ready as " & $r.user
  info "Invite link: " & r.user.id.createBotInvite(requiredPermissionsBot)

cl.events.message_create = proc (s: Shard, m: Message) {.async.} =
  if m.author.bot: return
  let cmds = m.content.split(" ")
  case cmds[0]
  of "!help":
    info (await originInfo(m, s)) & ": " & cmds[0]
    asyncCheck cl.api.send(
      m.channel_id,
      "Use `statusCommand [-v] servername` to query the status of an NWN server.\n" &
      "\"statusCommand\" can be one of: `" & statusCommands.join("`, `") & "`.\n" &
      "Add `-v` directly after the command for more details. Omitting the servername will query the default server. " &
      "Change or set the default server via `!default servername`. The default server is set per discord server/guild.\n" &
      "`!invitelink` shows a link to invite this bot to a server."
    )
  of "!invitelink":
    info (await originInfo(m, s)) & ": " & cmds[0]
    asyncCheck cl.api.send(m.channel_id, "Invite link for **" & s.user.username & "**: <" & s.user.id.createBotInvite(requiredPermissionsBot) & ">")
  of "!default":
    if m.guild_id.isSome:
      if cmds.len == 1:
        let defaultServer = defaultServers.getOrDefault(m.guild_id.get)
        if defaultServer != "":
          asyncCheck cl.api.send(m.channel_id, "Default server is: \"**" & defaultServer & "**\".")
        else:
          asyncCheck cl.api.send(m.channel_id, "Default server is not set.")
      else:
        let
          guild = await m.guild_id.get.getGuild(s)
          member = await s.getGuildMember(m.guild_id.get, m.author.id)
          perms = guild.computePerms(member)
        if cast[int](perms).permCheck(PermObj(allowed: requiredPermissionsSetDefault)):
          let defaultServer = cmds[1 .. ^1].join(" ")
          info (await originInfo(m, s)) & &": Setting default server to \"{defaultServer}\"."
          defaultServers[m.guild_id.get] = defaultServer
          writeFile(defaultServersFile, defaultServers.toJSON.pretty)
          asyncCheck cl.api.send(m.channel_id, &"Default server set to \"**{defaultServer}**\".")
        else:
          asyncCheck cl.api.send(
            m.channel_id,
            "Insufficient permissions to set the default server. Required permissions: `" & requiredPermissionsSetDefault.toSeq.join("`, `") & "`."
          )
    else:
      asyncCheck cl.api.send(m.channel_id, "Default server can only be set for server/guild channels.")
  of statusCommands:
    let verbose = cmds.len >= 2 and cmds[1] == "-v"
    if cmds.len == 1 or (verbose and cmds.len == 2): # request for default server
      if m.guild_id.isSome:
        let defaultServer = defaultServers.getOrDefault(m.guild_id.get)
        if defaultServer != "":
          info (await originInfo(m, s)) & &": Status request for default server \"{defaultServer}\" (verbose: {verbose})"
          asyncCheck getServerInfoAndReply(defaultServer, m, verbose)
        else:
          asyncCheck cl.api.send(m.channel_id, "No default server set.")
      else:
        asyncCheck cl.api.send(m.channel_id, "No default server available for this channel.")
    else: # request with server name
      let serverName = cmds[(if verbose: 2 else: 1) .. ^1].join(" ")
      info (await originInfo(m, s)) & &": Status request for server \"{serverName}\" (verbose: {verbose})"
      asyncCheck getServerInfoAndReply(serverName, m, verbose)

# Connect to Discord and run the bot
waitFor cl.startSession()

proc send (api: RestApi, channel_id: string, content = "", embed = none Embed): Future[Option[Message]] {.async.} =
  try:
    result = some await api.sendMessage(channel_id, content, embed = embed)
  except:
    error "Error sending discord message."
    let e = getCurrentException()
    error "Exception: ", e.name, ": ", e.msg

proc getServerInfoAndReply (serverName: string, m: Message, verbose = false) {.async.} =
  let server = await getServerInfo(serverName)
  if server.isSome:
    info &"Server \"{server.get.session_name}\" found"
    asyncCheck cl.api.send(m.channel_id, embed = some server.get.toEmbed(verbose))
  else:
    asyncCheck cl.api.send(m.channel_id, "Server \"" & serverName & "\" not found.")

var
  servers = parseJson("[]")
  lastCheck: Time

proc getServerInfo (name: string): Future[Option[Server]] {.async.} =
  if (getTime() - lastCheck) > initDuration(seconds = 30):
    lastCheck = getTime()
    servers = await getServerInfoFromAPI()
    # sort servers by current player count and server name
    servers.elems.sort do (a, b: JsonNode) -> int:
      result = b["current_players"].getInt - a["current_players"].getInt
      if result == 0:
        result = cmpIgnoreCase(a["session_name"].getStr, b["session_name"].getStr)
  for server in servers:
    if server["session_name"].getStr.toLower == name.toLower:
      return some server.to(Server)
  # if no server was found try again with only part of the name
  for server in servers:
    if name.toLower in server["session_name"].getStr.toLower:
      return some server.to(Server)

proc getServerInfoFromAPI (): Future[JsonNode] {.async.} =
  info "Calling Beamdog API"
  let client = newAsyncHttpClient()
  var response = "[]"
  try:
    response = await client.getContent("https://api.nwn.beamdog.net/v1/servers")
  except:
    error "Error calling the Beamdog API"
    let e = getCurrentException()
    error "Exception: ", e.name, ": ", e.msg
  result = parseJson(response)

proc toEmbed (server: Server, verbose = false): Embed =
  result.title = some server.session_name
  result.color = if server.passworded: some colRed.int else: some colGreen.int
  var fields = @[
    EmbedField(name: "Module", value: server.module_name, inline: some true),
    EmbedField(name: "Passworded", value: server.passworded.toYesNo, inline: some true),
    EmbedField(name: "Players", value: &"{server.current_players} / {server.max_players}", inline: some true),
    EmbedField(name: "Status from", value: $(getTime().toUnix - server.last_advertisement) & "s ago", inline: some true),
  ]
  if verbose:
    fields &= @[
      EmbedField(name: "Min. level", value: $server.min_level, inline: some true),
      EmbedField(name: "Max level", value: $server.max_level, inline: some true),
      EmbedField(name: "PvP", value: $server.pvp.PvP, inline: some true),
      EmbedField(name: "One party", value: server.one_party.toYesNo, inline: some true),
      EmbedField(name: "Servervault", value: server.servervault.toYesNo, inline: some true),
      EmbedField(name: "ILR", value: server.ilr.toYesNo, inline: some true),
      EmbedField(name: "ELC", value: server.elc.toYesNo, inline: some true),
      EmbedField(name: "Game type", value: $server.game_type.GameType, inline: some true),
      EmbedField(name: "Language", value: $server.language.Language, inline: some true),
      EmbedField(name: "Latency", value: $server.latency & "ms", inline: some true),
      EmbedField(name: "Host", value: server.host, inline: some true),
      EmbedField(name: "Port", value: $server.port, inline: some true),
      EmbedField(name: "Build", value: server.build & "." & $server.rev, inline: some true),
      EmbedField(name: "OS", value: $server.os.OS, inline: some true),
    ]
    if server.nwsync.isSome:
      let nwsync = server.nwsync.get
      if nwsync.manifests.len == 1 and nwsync.manifests[0].required:
        let manifestURL = nwsync.url & "/manifests/" & nwsync.manifests[0].hash & ".json"
        fields.add(EmbedField(name: "NWSync", value: &"[{nwsync.url}]({manifestURL})"))
    fields.add(EmbedField(name: "Description", value: server.module_description))
  # empty values in embeds are not permitted by the discord api
  result.fields = some fields.filterIt(not it.value.isEmptyOrWhitespace)

proc originInfo (m: Message, s: Shard): Future[string] {.async.} =
  if m.guild_id.isSome:
    result.add "[" & (await m.guild_id.get.getGuild(s)).name & "]"
  else:
    result.add "[]"
  if s.cache.guildChannels.hasKey(m.channel_id):
    result.add "[" & s.cache.guildChannels[m.channel_id].name & "]"
  else:
    result.add "[]"
  result.add &"[{m.author}]"

proc getGuild (guild_id: string, s: Shard): Future[Guild] {.async.} =
  if s.cache.guilds.hasKey(guild_id):
    return s.cache.guilds[guild_id]
  let guild = await cl.api.getGuild(guild_id)
  s.cache.guilds[guild_id] = guild
  return guild

proc getGuildMember(s: Shard, guild_id, user_id: string): Future[Member] {.async.} =
  var member: Member
  var waiting = true
  await s.requestGuildMembers(guild_id, presences = true, user_ids = @[user_id])
  cl.events.guild_members_chunk = proc (s: Shard, g: Guild, e: GuildMembersChunk) {.async.} =
    if e.members.len == 0:
        raise newException(Exception, "Member was not found.")
    member = e.members[0]
    if member == nil:
        raise newException(Exception, "Member was not found.")
    waiting = false
  while member == nil:
    poll()
  return member

proc toJSON (t: Table[string, string]): JsonNode =
  result = newJArray()
  for guild_id, server in t.pairs:
    let o = newJObject()
    o.add("guild_id", guild_id.newJString)
    o.add("server", server.newJString)
    result.add(o)

proc toYesNo (b: bool): string =
  if b: "Yes" else: "No"
