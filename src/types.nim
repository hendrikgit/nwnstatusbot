import options

type
  NWSyncManifest* = object
    required*: bool
    hash*: string

  NWSync* = object
    url*: string
    manifests*: seq[NWSyncManifest]

  Server* = object
    first_seen*: int
    last_advertisement*: int
    session_name*: string
    module_name*: string
    module_description*: string
    passworded*: bool
    min_level*: int
    max_level*: int
    current_players*: int
    max_players*: int
    build*: string
    rev*: int
    pvp*: int
    servervault*: bool
    elc*: bool
    ilr*: bool
    one_party*: bool
    player_pause*: bool
    os*: int
    language*: int
    game_type*: int
    latency*: int
    host*: string
    port*: int
    nwsync*: Option[NWSync]
    kx_pk*: Option[string]
    sign_pk*: Option[string]

  PvP* = enum
    NoPvP = "No PvP"
    PartyPvP = "Party PvP"
    FullPvP = "Full PvP"

  GameType* = enum
    Action
    Story
    StoryLite = "Story Lite"
    RolePlay = "Role Play"
    Team
    Melee
    Arena
    Social
    Alternative
    PWAction = "PW Action"
    PWStory = "PW Story"
    Solo
    TechSupport = "Tech Support"

  Language* = enum
    English = 0
    French = 1
    German = 2
    Italian = 3
    Spanish = 4
    Polish = 5
    Korean = 128
    ChineseTraditional = (129, "Chinese (traditional)")
    ChineseSimplified = (130, "Chinese (simplified)")
    Japanese = 131

  OS* = enum
    WindowsX86 = (1, "Windows (x86)")
    WindowsX64 = (2, "Windows (x64)")
    LinuxX86 = (10, "Linux (x86)")
    LinuxX64 = (11, "Linux (x64)")
    LinuxArm32 = (12, "Linux (arm32)")
    LinuxArm64 = (13, "Linux (arm64)")
    MacX86 = (20, "Mac (x86)")
    MacX64 = (21, "Mac (x64)")
    IOS = (30, "iOS")
    AndroidArm32 = (40, "Android (arm32)")
    AndroidArm64 = (41, "Android (arm64)")
    AndroidX64 = (42, "Android (x64)")
